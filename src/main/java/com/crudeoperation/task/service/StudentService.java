package com.crudeoperation.task.service;

import com.crudeoperation.task.entity.Student;
import com.crudeoperation.task.Exception.RecordNotFoundException;

import java.util.List;

public interface StudentService {
    List<Student> getStudent() throws RecordNotFoundException;

    Student getStudentById(Integer id) throws RecordNotFoundException, RecordNotFoundException;

    Student createStudent(Student student);

    void deleteStudentById(Integer id) throws RecordNotFoundException;
}
