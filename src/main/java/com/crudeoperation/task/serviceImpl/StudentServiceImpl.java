package com.crudeoperation.task.serviceImpl;

import com.crudeoperation.task.Exception.RecordNotFoundException;
import com.crudeoperation.task.entity.Student;
import com.crudeoperation.task.repository.StudentRepo;
import com.crudeoperation.task.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepo studentRepo;

    @Override
    public List<Student> getStudent() throws RecordNotFoundException {

        List<Student> students = studentRepo.findAll();
        if (students.size() > 0) {
            return students;
        } else {
            return (List<Student>) new  RecordNotFoundException("no student are avalilable");
        }

    }

    @Override
    public Student getStudentById(Integer id) throws RecordNotFoundException {

        Optional<Student> student = studentRepo.findById(id);

        if (student.isPresent()) {
            return student.get();
        } else {
            throw new RecordNotFoundException("no student are available");
        }
    }

    @Override
    public Student createStudent(Student student) {
        if (student.getId() == null) {
            student = studentRepo.save(student);
            return student;
        } else {
            Optional<Student> student1 = studentRepo.findById(student.getId());
            if (student1.isPresent()) {
                Student student2 = student1.get();
                student2.setName(student.getName());
                student2.setLastName(student.getLastName());
                student2.setAddress(student.getAddress());
                student2.setEmail(student.getEmail());
                student2.setAddress(student.getAddress());
                student2.setPassword(student.getPassword());
                student2 = studentRepo.save(student2);
                return student2;
            } else {
                student = studentRepo.save(student);
                return student;
            }
        }

    }

    @Override
    public void deleteStudentById(Integer id) throws RecordNotFoundException {
        Optional<Student> student = studentRepo.findById(id);
        if (student.isPresent()) {
            studentRepo.deleteById(id);
        } else {
            throw new RecordNotFoundException("no record exits for given id");
        }

    }
}
