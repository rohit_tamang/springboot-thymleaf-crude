package com.crudeoperation.task.Exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class RecordNotFoundException extends Exception {
    private String massag;

    public RecordNotFoundException(String massag) {
        this.massag = massag;
    }

    public RecordNotFoundException(String message, String massag) {
        super(message);
        this.massag = massag;
    }

    public RecordNotFoundException(String message, Throwable cause, String massag) {
        super(message, cause);
        this.massag = massag;
    }

    public RecordNotFoundException(Throwable cause, String massag) {
        super(cause);
        this.massag = massag;
    }

    public RecordNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String massag) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.massag = massag;
    }
}
