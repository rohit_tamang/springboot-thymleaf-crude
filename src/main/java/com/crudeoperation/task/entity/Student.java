package com.crudeoperation.task.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "last_Name")
    private String lastName;
    @Column(name = "address")
    private String address;
    @Column(name = "email_adress")
    private String email;
    @Column(name = "password")
    private String password;


}
