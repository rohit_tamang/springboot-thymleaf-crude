package com.crudeoperation.task.controller;

import com.crudeoperation.task.entity.Student;
import com.crudeoperation.task.Exception.RecordNotFoundException;
import com.crudeoperation.task.serviceImpl.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class StudentController {
    @Autowired
    private StudentServiceImpl studentService;

    @GetMapping("/")
    public String getStudentList(Model model) throws RecordNotFoundException {
        List<Student> students = studentService.getStudent();
        model.addAttribute("studentlist", students);
        return "student-list";
    }


    @RequestMapping(path = {"/edit", "/edit/{id}"})
    public String editEmplyoeeById(Model model, @PathVariable("id") Optional<Integer> id) throws RecordNotFoundException {
        if (id.isPresent()) {
            Student student = studentService.getStudentById(id.get());
            model.addAttribute("student", student);
        } else {
            model.addAttribute("student", new Student());
        }
        return "add-student";
    }

    @PostMapping("/save-student")
    public String saveStudent(Student student) {
        studentService.createStudent(student);
        return "redirect:/";
    }

    @RequestMapping("/delete-student/{id}")
    public String deleleById(@PathVariable("id") Integer id) throws RecordNotFoundException {

        studentService.deleteStudentById(id);
        return "redirect:/";

    }


}
