package com.crudeoperation.task.repository;

import com.crudeoperation.task.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepo extends JpaRepository<Student,Integer> {
}
